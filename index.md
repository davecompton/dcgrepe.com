---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: home
title: Home
---
This IN group is for anyone interested in Linux and talking about it with your friends and neighbors.
We are having bi-weekly meetings via Jitsi now due to COVID-19. You can find out more information on our [Get Together](https://gettogether.community/wsealug/){:target="\_blank"} page or join us on our official Matrix Channel [#wsealug-general:seattlematrix.org](https://matrix.to/#/#wsealug-general:seattlematrix.org){:target="\_blank"}.

At the meetings, we either have a ['Lean-Coffee'](http://leancoffee.org/){:target="\_blank"} style of a meeting, or one of our members will give a 30-45 minute demonstration of something GNU/Linux related. We are keeping track of these and suggestions on a
public [Wekan board](https://wekan.seattlematrix.org/b/NF7Q56SoHQFsN5rNt/wsealug-talk-ideas){:target="\_blank"}.

We hope to have recordings and slide-decks of past presentations, stay tuned!

Thanks for checking us out, and we hope to see you at the next meet up.

WSeaLUG Leadership Team!
